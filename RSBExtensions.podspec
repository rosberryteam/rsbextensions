Pod::Spec.new do |s|

    s.name             = "RSBExtensions"
    s.version          = "0.1.1"
    s.summary          = "The useful tools for developing."
    s.homepage         = "https://bitbucket.org/rosberryteam/rsbextensions"
    s.license          = "MIT"
    s.author           = { "Nikita Ermolenko" => "nikita.ermolenko@rosberry.com" }
    s.source           = { :git => "https://bitbucket.org/rosberryteam/rsbextensions.git", :tag => 'v0.1.0' }
    s.source_files     = 'Classes/RSBExtensions/**/*.{h,m}'

    s.platform     = :ios, '7.0'
    s.requires_arc = true

    s.subspec 'RSBKeyboardAutoScroll' do |ss|
        ss.source_files = 'Classes/RSBExtensions/RSBKeyboardAutoScroll/*.{h,m}'
        ss.requires_arc = false
    end

end
