//
//  UIScrollView+RSBKeyboardAutoScroll.h
//  AutoScroll
//
//  Created by Nikita on 27/01/16.
//  Copyright © 2016 Nikita. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIScrollView (RSBKeyboardAutoScroll)

@property (nonatomic, getter=isAutoScrollEnable) IBInspectable BOOL autoScrollEnable;

///Needed for setting your own bottom inset. 
///Default bottom inset equals difference bounds between keyboard and scrollview
@property (weak, nonatomic, nullable) UIView *bottomView;

@end
