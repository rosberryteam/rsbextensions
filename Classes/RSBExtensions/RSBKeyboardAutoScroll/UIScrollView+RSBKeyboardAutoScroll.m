//
//  UIScrollView+RSBKeyboardAutoScroll.m
//  AutoScroll
//
//  Created by Nikita on 27/01/16.
//  Copyright © 2016 Nikita. All rights reserved.
//

#import "UIScrollView+RSBKeyboardAutoScroll.h"

#import <objc/runtime.h>

@interface UIScrollView ()

@property (nonatomic) UIEdgeInsets currentInsets;
@property (nonatomic) UIEdgeInsets currentScrollIndicatorInsets;
@property (nonatomic) BOOL currentlyScrollEnabled;
@property (nonatomic, getter=isKeyboardHidden) BOOL keyboardHidden;

@end

@implementation UIScrollView (RSBKeyboardAutoScroll)

+ (void)load {
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        [self swizzleOriginalSelector:NSSelectorFromString(@"dealloc") withSelector:@selector(rsb_kbd_autoscroll_swizzled_dealloc)];
    });
}

+ (void)swizzleOriginalSelector:(SEL)originalSelector withSelector:(SEL)swizzledSelector {
    
    Class class = [self class];
    
    Method originalMethod = class_getInstanceMethod(class, originalSelector);
    Method swizzledMethod = class_getInstanceMethod(class, swizzledSelector);
    
    BOOL didAddMethod = class_addMethod(class,originalSelector,method_getImplementation(swizzledMethod), method_getTypeEncoding(swizzledMethod));
    if (didAddMethod) {
        class_replaceMethod(class, swizzledSelector, method_getImplementation(originalMethod), method_getTypeEncoding(originalMethod));
    } else {
        method_exchangeImplementations(originalMethod, swizzledMethod);
    }
}

#pragma mark - Swizzled methods

- (void)rsb_kbd_autoscroll_swizzled_dealloc {
    
    if (self.isAutoScrollEnable) {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
        [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    }
    [self rsb_kbd_autoscroll_swizzled_dealloc];
}

#pragma mark - Setters / Getters

- (void)setAutoScrollEnable:(BOOL)autoScrollEnable {
    
    if (autoScrollEnable && !self.isAutoScrollEnable) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShowNotification:) name:UIKeyboardWillShowNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHideNotification:) name:UIKeyboardWillHideNotification object:nil];
        
    } else if (!autoScrollEnable && self.isAutoScrollEnable) {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
        [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    }
    
    objc_setAssociatedObject(self, @selector(isAutoScrollEnable), @(autoScrollEnable), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (BOOL)isAutoScrollEnable {
    return [objc_getAssociatedObject(self, @selector(isAutoScrollEnable)) boolValue];
}

- (void)setCurrentInsets:(UIEdgeInsets)currentInsets {
    objc_setAssociatedObject(self, @selector(currentInsets), [NSValue valueWithUIEdgeInsets:currentInsets], OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (UIEdgeInsets)currentInsets {
    return [objc_getAssociatedObject(self, @selector(currentInsets)) UIEdgeInsetsValue];
}

- (void)setCurrentScrollIndicatorInsets:(UIEdgeInsets)currentScrollIndicatorInsets {
    objc_setAssociatedObject(self, @selector(currentScrollIndicatorInsets), [NSValue valueWithUIEdgeInsets:currentScrollIndicatorInsets], OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (UIEdgeInsets)currentScrollIndicatorInsets {
    return [objc_getAssociatedObject(self, @selector(currentScrollIndicatorInsets)) UIEdgeInsetsValue];
}

- (void)setCurrentlyScrollEnabled:(BOOL)currentlyScrollEnabled {
    objc_setAssociatedObject(self, @selector(currentlyScrollEnabled), @(currentlyScrollEnabled), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (BOOL)currentlyScrollEnabled {
    return [objc_getAssociatedObject(self, @selector(currentlyScrollEnabled)) boolValue];
}

- (void)setKeyboardHidden:(BOOL)keyboardHidden {
    objc_setAssociatedObject(self, @selector(isKeyboardHidden), @(keyboardHidden), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (BOOL)isKeyboardHidden {
    id object = objc_getAssociatedObject(self, @selector(isKeyboardHidden));
    if (!object) {
        return YES;
    }
    return [object boolValue];
}

- (void)setBottomView:(UIView *)bottomView {
    objc_setAssociatedObject(self, @selector(bottomView), bottomView, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (UIView *)bottomView {
    return objc_getAssociatedObject(self, @selector(bottomView));
}

#pragma mark - Additionals

- (UIView *)firstResponderViewInView:(UIView *)view {
    
    if (view.isFirstResponder) {
        return view;
    }
    for (UIView *subview in view.subviews) {
        UIView *firstResponderView = [self firstResponderViewInView:subview];
        if (firstResponderView) {
            return firstResponderView;
        }
    }
    return nil;
}

#pragma mark - Notifications

- (void)keyboardWillShowNotification:(NSNotification*)notification {

    if (self.isKeyboardHidden) {
        self.currentlyScrollEnabled = self.scrollEnabled;
        self.currentInsets = self.contentInset;
        self.currentScrollIndicatorInsets = self.scrollIndicatorInsets;
    }
    self.keyboardHidden = NO;

    NSDictionary *userInfo = [notification userInfo];
    CGRect keyboardFrame = [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    CGRect windowRectForScrollView = [self.superview convertRect:self.frame toView:nil];
    
    CGFloat yInset;
    if (self.bottomView) {
        CGRect rectForBottomView = [self.superview convertRect:self.bottomView.frame toView:nil];
        yInset = CGRectGetMaxY(rectForBottomView) - CGRectGetMinY(keyboardFrame);
    } else {
        yInset = CGRectGetMaxY(windowRectForScrollView) - CGRectGetMinY(keyboardFrame);
    }

    if (yInset <= 0) {
        return;
    }
    
    UIView *firstResponderView = [self firstResponderViewInView:self];
    CGRect firstResponderViewRectDependsWindow = [firstResponderView.superview convertRect:firstResponderView.frame toView:nil];

    UIEdgeInsets contentInset = UIEdgeInsetsMake(self.contentInset.top, self.contentInset.left, yInset, self.contentInset.right);

    self.scrollEnabled = YES;
    self.contentInset = contentInset;

    if (CGRectGetMinY(firstResponderViewRectDependsWindow) - self.contentInset.top < 0) {
        self.contentOffset = CGPointMake(0, self.contentOffset.y - (self.contentInset.top - CGRectGetMinY(firstResponderViewRectDependsWindow)));
    } else {
        CGFloat offset = CGRectGetMinY(keyboardFrame) - CGRectGetMaxY(firstResponderViewRectDependsWindow);
        if (offset < 0) {
            offset = -offset;
            CGFloat topInsetForFirstResponderView = CGRectGetMinY(firstResponderViewRectDependsWindow) - self.contentInset.top;
            if (offset > topInsetForFirstResponderView) {
                offset = topInsetForFirstResponderView;
            }
            self.contentOffset = CGPointMake(0, self.contentOffset.y + offset);
        }
    }

    self.scrollIndicatorInsets = UIEdgeInsetsMake(self.scrollIndicatorInsets.top, self.scrollIndicatorInsets.left, contentInset.bottom, self.scrollIndicatorInsets.right);
}

- (void)keyboardWillHideNotification:(NSNotification*)notification {

    if (!self.isKeyboardHidden) {
        self.contentInset = self.currentInsets;
        self.scrollIndicatorInsets = self.currentScrollIndicatorInsets;
        self.scrollEnabled = self.currentlyScrollEnabled;
    }
    self.keyboardHidden = YES;
}

@end
