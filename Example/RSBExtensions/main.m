//
//  main.m
//  RSBExtensions
//
//  Created by Nikita Ermolenko on 02/04/2016.
//  Copyright (c) 2016 Nikita Ermolenko. All rights reserved.
//

@import UIKit;
#import "RSBAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([RSBAppDelegate class]));
    }
}
