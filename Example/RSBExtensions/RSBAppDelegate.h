//
//  RSBAppDelegate.h
//  RSBExtensions
//
//  Created by Nikita Ermolenko on 02/04/2016.
//  Copyright (c) 2016 Nikita Ermolenko. All rights reserved.
//

@import UIKit;

@interface RSBAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
