//
//  RSBTableViewInsetsViewController.m
//  RSBAutoKeyboardScroll
//
//  Created by Nikita on 2/2/16.
//  Copyright © 2016 Nikita. All rights reserved.
//

#import "RSBTableViewInsetsViewController.h"
#import "RSBTextViewTableViewCell.h"

#import <RSBExtensions/UIScrollView+RSBKeyboardAutoScroll.h>

@interface RSBTableViewInsetsViewController () <UITableViewDelegate, UITableViewDataSource, UITextViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation RSBTableViewInsetsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.autoScrollEnable = YES;
    
//    [self.tableView setScrollIndicatorInsets:UIEdgeInsetsMake(50, 50, 50, 50)];
    [self.tableView setScrollIndicatorInsets:UIEdgeInsetsZero];
    [self.tableView setContentInset:UIEdgeInsetsMake(50, 50, 100, 50)];
    self.tableView.backgroundColor = [UIColor redColor];
    
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([RSBTextViewTableViewCell class]) bundle:nil]
         forCellReuseIdentifier:NSStringFromClass([RSBTextViewTableViewCell class])];
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 80.0f;
}

#pragma mark - UITableViewDataSource

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    RSBTextViewTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([RSBTextViewTableViewCell class]) forIndexPath:indexPath];
    cell.textView.delegate = self;
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 20;
}

#pragma mark - UITextViewDelegate

- (BOOL)textView:(UITextView*) textView shouldChangeTextInRange: (NSRange) range replacementText: (NSString*) text {
    
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    return YES;
}

@end
