//
//  RSBTextViewTableViewCell.h
//  RSBAutoKeyboardScroll
//
//  Created by Nikita on 2/2/16.
//  Copyright © 2016 Nikita. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RSBTextViewTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UITextView *textView;

@end
