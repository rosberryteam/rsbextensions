//
//  RSBTableViewTextViewViewController.m
//  RSBAutoKeyboardScroll
//
//  Created by Nikita on 2/2/16.
//  Copyright © 2016 Nikita. All rights reserved.
//

#import "RSBTableViewTextViewViewController.h"
#import "RSBTextViewTableViewCell.h"

#import <RSBExtensions/UIScrollView+RSBKeyboardAutoScroll.h>

@interface RSBTableViewTextViewViewController () <UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation RSBTableViewTextViewViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tableView.contentInset = UIEdgeInsetsMake(64, 0, 0, 0);
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.autoScrollEnable = YES;
    
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([RSBTextViewTableViewCell class]) bundle:nil]
         forCellReuseIdentifier:NSStringFromClass([RSBTextViewTableViewCell class])];
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 300.0f;
}

#pragma mark - UITableViewDataSource

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    RSBTextViewTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([RSBTextViewTableViewCell class]) forIndexPath:indexPath];
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 20;
}

@end
