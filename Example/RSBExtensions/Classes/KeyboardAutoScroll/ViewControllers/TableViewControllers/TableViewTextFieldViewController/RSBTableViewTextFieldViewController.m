//
//  RSBTableViewTextFieldViewController.m
//  RSBAutoKeyboardScroll
//
//  Created by Nikita on 2/2/16.
//  Copyright © 2016 Nikita. All rights reserved.
//

#import "RSBTableViewTextFieldViewController.h"
#import "RSBTextFieldTableViewCell.h"

#import <RSBExtensions/UIScrollView+RSBKeyboardAutoScroll.h>

@interface RSBTableViewTextFieldViewController () <UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation RSBTableViewTextFieldViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.autoScrollEnable = YES;
    
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([RSBTextFieldTableViewCell class]) bundle:nil]
         forCellReuseIdentifier:NSStringFromClass([RSBTextFieldTableViewCell class])];
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 40.0f;
}

#pragma mark - UITableViewDataSource

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    RSBTextFieldTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([RSBTextFieldTableViewCell class]) forIndexPath:indexPath];
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 20;
}

@end
