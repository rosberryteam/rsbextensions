//
//  RSBScrollWithButtonViewController.m
//  RSBAutoKeyboardScroll
//
//  Created by Nikita on 2/2/16.
//  Copyright © 2016 Nikita. All rights reserved.
//

#import "RSBScrollWithButtonViewController.h"

#import <RSBExtensions/UIScrollView+RSBKeyboardAutoScroll.h>

@interface RSBScrollWithButtonViewController ()

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIButton *button;

@end

@implementation RSBScrollWithButtonViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.scrollView.bottomView = self.button;
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
