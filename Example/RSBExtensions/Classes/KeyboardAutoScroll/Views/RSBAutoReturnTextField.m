//
//  RSBAutoReturnTextField.m
//  RSBAutoKeyboardScroll
//
//  Created by Nikita on 2/2/16.
//  Copyright © 2016 Nikita. All rights reserved.
//

#import "RSBAutoReturnTextField.h"

@interface RSBAutoReturnTextField () <UITextFieldDelegate>

@end

@implementation RSBAutoReturnTextField

- (void)awakeFromNib {
    self.delegate = self;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

@end
