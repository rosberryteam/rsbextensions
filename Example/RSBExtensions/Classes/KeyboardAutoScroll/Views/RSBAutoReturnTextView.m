//
//  RSBAutoReturnTextView.m
//  RSBAutoKeyboardScroll
//
//  Created by Nikita on 2/2/16.
//  Copyright © 2016 Nikita. All rights reserved.
//

#import "RSBAutoReturnTextView.h"

@interface RSBAutoReturnTextView () <UITextViewDelegate>

@end

@implementation RSBAutoReturnTextView

- (void)awakeFromNib {
    self.delegate = self;
}

- (BOOL)textView:(UITextView*) textView shouldChangeTextInRange: (NSRange) range replacementText: (NSString*) text {
    
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    return YES;
}

@end
