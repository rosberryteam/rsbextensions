//
//  RSBMainSegueModel.h
//  RSBExtensions
//
//  Created by Nikita on 2/5/16.
//  Copyright © 2016 Nikita Ermolenko. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RSBMainSegueModel : NSObject

@property (nonatomic, nonnull) NSString *name;
@property (nonatomic, nonnull) NSString *segueIdentifier;

@end
