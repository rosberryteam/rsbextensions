//
//  RSBMainViewControllerFactory.m
//  RSBExtensions
//
//  Created by Nikita on 2/5/16.
//  Copyright © 2016 Nikita Ermolenko. All rights reserved.
//

#import "RSBMainViewControllerFactory.h"

#import "RSBMainSegueModel.h"
#import "RSBSegues.h"

@interface RSBMainViewControllerFactory ()

@property (nonatomic, nonnull) NSArray <RSBMainSegueModel *> *segueModels;

@end

@implementation RSBMainViewControllerFactory

+ (nullable instancetype)factory {
    
    static RSBMainViewControllerFactory *factory = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        factory = [[RSBMainViewControllerFactory alloc] init];
        factory.segueModels = [self segueModels];
    });
    return factory;
}

+ (NSArray <RSBMainSegueModel *> *)segueModels {
    
    NSMutableArray <RSBMainSegueModel *> *models = [[NSMutableArray alloc] init];
    
    RSBMainSegueModel *keyboardAutoScrollModel = [[RSBMainSegueModel alloc] init];
    keyboardAutoScrollModel.name = @"Keyboard Scroll Category";
    keyboardAutoScrollModel.segueIdentifier = RSBSegueIdentifierShowKeyboardAutoScrollStoryboard;
    
    [models addObject:keyboardAutoScrollModel];
    return models;
}

@end
