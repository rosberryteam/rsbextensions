//
//  RSBMainViewControllerFactory.h
//  RSBExtensions
//
//  Created by Nikita on 2/5/16.
//  Copyright © 2016 Nikita Ermolenko. All rights reserved.
//

@class RSBMainSegueModel;

@interface RSBMainViewControllerFactory : NSObject

@property (nonatomic, nonnull, readonly) NSArray <RSBMainSegueModel *> *segueModels;

+ (nullable instancetype)factory;

@end
