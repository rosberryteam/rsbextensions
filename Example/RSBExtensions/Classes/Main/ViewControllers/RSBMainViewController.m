//
//  RSBMainViewController.m
//  RSBExtensions
//
//  Created by Nikita on 2/5/16.
//  Copyright © 2016 Nikita Ermolenko. All rights reserved.
//

#import "RSBMainViewController.h"
#import "RSBMainViewControllerTableViewCell.h"

#import "RSBMainSegueModel.h"
#import "RSBMainViewControllerFactory.h"

@interface RSBMainViewController () <UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, nonnull) NSArray <RSBMainSegueModel *> *segueModels;

@end

@implementation RSBMainViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];

    self.segueModels = [RSBMainViewControllerFactory factory].segueModels;
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([RSBMainViewControllerTableViewCell class]) bundle:nil]
         forCellReuseIdentifier:NSStringFromClass([RSBMainViewControllerTableViewCell class])];
}

#pragma mark - Protocols

#pragma mark UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 50.0f;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self performSegueWithIdentifier:self.segueModels[indexPath.row].segueIdentifier sender:nil];
}

#pragma mark UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return self.segueModels.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    RSBMainViewControllerTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([RSBMainViewControllerTableViewCell class])
                                                                               forIndexPath:indexPath];
    cell.nameLabel.text = self.segueModels[indexPath.row].name;
    return cell;
}

@end
