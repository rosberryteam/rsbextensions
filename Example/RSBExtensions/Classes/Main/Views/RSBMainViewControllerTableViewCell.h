//
//  RSBMainViewControllerTableViewCell.h
//  RSBExtensions
//
//  Created by Nikita on 2/5/16.
//  Copyright © 2016 Nikita Ermolenko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RSBMainViewControllerTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;

@end
